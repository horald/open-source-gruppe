Hallo liebe Open-Source-Freunde,

ich freue mich, dass sich schon über 10 Mitglieder in meiner Meetup-Gruppe angemeldet haben. Um Spam-Mitgliedschaften zu vermeiden, habe ich die Aufnahme-Bedingungen einwenig verschärft. Neue Mitglieder müssen jetzt kurz begründen, warum sie in die Gruppe möchten. Bei mindestens 5 Teilnehmern, denke ich, wäre ein erstes Treffen sinnvoll. Auf meine letzte Doodle-Anfrage kamen aber leider nur max. 3 Teilnehmer zustande. Bevor ich eine neue Doodle-Anfrage starte, frage ich nochmal in die Runde wer ernsthaft Interesse hat und welche Tage in Frage kommen, so dass ich die Doodle-Anfrage besser eingrenzen kann.
Auch der Treffpunkt steht noch nicht fest. Falls jemand Räumlichkeiten anbieten kann, bitte ich um eine kurze Rückmeldung. Ansonsten wollte ich folgende Lokalitäten zur Auswahl stellen:<br><br>
- [http://www.cafe-helmholtz.de/](http://www.cafe-helmholtz.de/) (allerdings montags geschlossen)<br>
- [https://www.potpourri.koeln/](https://www.potpourri.koeln/) (anscheinend kein WLan mehr)<br>
- [https://www.koeln-hostel.de/](https://www.koeln-hostel.de/?page_id=4748) (Weltempfänger, WLan über Freifunk)<br>
<br><br>
Mir ist wichtig, dass der Fokus der Gruppe auf Teamwork steht. Daher würde ich auch vorschlagen, dass unser erstes Projekt die Zusammenarbeit mit git sein sollte.
Docker und andere Tools mögen zwar sehr mächtig sein, aber ich möchte möglichst mit dem kleinesten gemeinsammen Nenner anfangen und da ist git meines Erachtens für den Anfang völlig ausreichend. Ich möchte auch Anfängern und Hobby-Programmieren eine Chance bieten und da sollte man nicht zuviel Wissen voraussetzen.
Leider habe ich mit git noch nicht richtig im Team gearbeitet, daher wollte ich anfragen, wer sich mit anlegen von branches und merges auskennt und der Gruppe eine kurze Einleitung geben kann. Über zahlreiche konstruktive Rückmeldungen würde ich mich sehr freuen.
<br><br>
Im Übrigen habe ich die Gruppe auch auf gettogehter gestartet, da es Open-Source ist: [open-source-developer](https://gettogether.community/open-source-developer/)<br>
Und Meetup hat ja wohl geplant demnächst auch Gebühren von Teilnehmern zu nehmen.

Viele Grüße
Horst Meyer